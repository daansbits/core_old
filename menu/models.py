import math
from django.db import models
from base.models import Restaurant


class Menu(models.Model):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.RESTRICT)
    title = models.CharField(max_length=100)
    notes = models.TextField(blank=True)

    def __str__(self):
        return f"{self.title} ({self.restaurant.name})"

    def get_categories(self):
        return Category.objects.filter(menu=self)


class Category(models.Model):
    name = models.CharField(max_length=200)
    menu = models.ForeignKey(Menu, on_delete=models.RESTRICT, blank=True, null=True)
    notes = models.TextField(blank=True)
    order = models.IntegerField(blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.order:
            try:
                previous_order_nr = Category.objects.filter(menu=self.menu).latest('order').order
                self.order = int((math.ceil(previous_order_nr / 10.0) + 1) * 10)
            except Category.DoesNotExist:
                self.order = 10
        super(Category, self).save(*args, **kwargs)

    def get_menu_items(self):
        return MenuItem.objects.filter(category=self)


class MenuItem(models.Model):
    register_code = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    table_price = models.IntegerField(blank=True, null=True)
    takeout_price = models.IntegerField(blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.RESTRICT, related_name='menu_items')
    description = models.TextField(blank=True)
    package_items = models.ManyToManyField("self", blank=True, symmetrical=False)

    def __str__(self):
        return str(self.category.menu.restaurant.name) + ' ' + self.name
