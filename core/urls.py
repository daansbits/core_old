"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework import routers

from api.viewsets import MenuItemViewSet, CategoryViewSet, MenuList, RestaurantViewSet, ConnectionTest, \
    RestaurantMenuIds

router = routers.SimpleRouter()
router.register(r'menu_items', MenuItemViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'restaurants', RestaurantViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('test/', ConnectionTest.as_view()),
    path('restaurant/<int:rest_id>/menu/<int:menu_id>/', MenuList.as_view()),
    path('restaurant/<int:rest_id>/menu/ids/', RestaurantMenuIds.as_view()),
]
urlpatterns += router.urls
