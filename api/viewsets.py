from rest_framework import viewsets, generics
from rest_framework.views import APIView

from .serializers import MenuItemSerializer, CategorySerializer, RestaurantSerializer, MenuSerializer
from menu.models import MenuItem, Category, Restaurant, Menu


class MenuItemViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = MenuItem.objects.all()
    serializer_class = MenuItemSerializer


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all().order_by('order')
    serializer_class = CategorySerializer


class RestaurantViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer


class MenuList(generics.ListAPIView):
    serializer_class = MenuSerializer

    def get_queryset(self):
        return Menu.objects.filter(pk=self.kwargs['menu_id'])


class RestaurantMenuIds(APIView):
    def get(self, request, rest_id):
        from django.http import HttpResponse

        menus = Menu.objects.filter(restaurant_id=rest_id).values('id')
        menu_list = []
        for menu in menus:
            menu_list.append(menu['id'])
        return HttpResponse(menu_list)


class ConnectionTest(APIView):
    def get(self):
        return {'result': True}
