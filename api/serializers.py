from rest_framework import serializers

from menu.models import MenuItem, Category, Restaurant, Menu


class PackagedItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuItem
        fields = ['name']


class MenuItemSerializer(serializers.ModelSerializer):
    package_items = PackagedItemSerializer(many=True)

    class Meta:
        model = MenuItem
        fields = ['name', 'table_price', 'takeout_price', 'description', 'package_items']


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = ['name',
                  'street', 'house', 'zipcode', 'city', 'phone',
                  'opening_notes',
                  'monday_opening', 'monday_closing',
                  'tuesday_opening', 'tuesday_closing',
                  'wednesday_opening', 'wednesday_closing',
                  'thursday_opening', 'thursday_closing',
                  'friday_opening', 'friday_closing',
                  'saturday_opening', 'saturday_closing',
                  'sunday_opening', 'sunday_closing',
                  ]


class CategorySerializer(serializers.ModelSerializer):
    menu_items = MenuItemSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['name', 'menu_items', 'notes', 'order']


class MenuSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True, read_only=True, source="get_categories")

    class Meta:
        model = Menu
        fields = ['title', 'categories']
