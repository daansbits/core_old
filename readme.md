# Setting up your local environment

1. Use PyCharm's _Get From Version Control_ to clone the repository
2. Go to the project Settings.
3. In the Project Interpreter section, click on the cogwheel button and add a new Virtual Environment.
4. Install the required packages: `pip3 install -r requirements.txt`
5. Run your migrations and create your database
6. Start the manage.py `createsuperuser` process