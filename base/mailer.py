from django.core.mail import send_mail


class Mailer:
    def test_mail(self):
        send_mail(
            subject="Test email",
            message="If you see this email, the test has succeeded.",
            from_email="dev@omniadicta.net",
            recipient_list=["dev@omniadicta.net"],
            fail_silently=False,
        )
