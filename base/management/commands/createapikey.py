from django.core.management import BaseCommand


class Command(BaseCommand):
    help = "Creates an API key for a specific restaurant id"

    def add_arguments(self, parser):
        parser.add_argument('restaurant_id', nargs='+', type=int)

    def handle(self, *args, **options):
        from base.models import Restaurant
        Restaurant.objects.get(pk=options['restaurant_id']).create_api_key()
