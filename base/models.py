from django.db import models
from django.contrib.auth.models import AbstractUser


class Chain(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Restaurant(models.Model):
    chain = models.ForeignKey(Chain, on_delete=models.PROTECT, blank=True, null=True)
    name = models.CharField(max_length=200)

    street = models.CharField(max_length=100, blank=True, null=True)
    house = models.CharField(max_length=10, blank=True, null=True)
    zipcode = models.CharField(max_length=6, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)

    monday_opening = models.TimeField(blank=True, null=True)
    monday_closing = models.TimeField(blank=True, null=True)
    tuesday_opening = models.TimeField(blank=True, null=True)
    tuesday_closing = models.TimeField(blank=True, null=True)
    wednesday_opening = models.TimeField(blank=True, null=True)
    wednesday_closing = models.TimeField(blank=True, null=True)
    thursday_opening = models.TimeField(blank=True, null=True)
    thursday_closing = models.TimeField(blank=True, null=True)
    friday_opening = models.TimeField(blank=True, null=True)
    friday_closing = models.TimeField(blank=True, null=True)
    saturday_opening = models.TimeField(blank=True, null=True)
    saturday_closing = models.TimeField(blank=True, null=True)
    sunday_opening = models.TimeField(blank=True, null=True)
    sunday_closing = models.TimeField(blank=True, null=True)
    opening_notes = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.pk:
            self.create_api_key()
        super(Restaurant, self).save(*args, **kwargs)

    def create_api_key(self):
        from rest_framework_api_key.models import APIKey
        api_key, key = APIKey.objects.create_key(name='{0}: {1}'.format(self.pk, self.name))
        from django.core.mail import send_mail
        send_mail(
            subject="New API Key created",
            message="The API key for {0}: {1} is {2}".format(self.pk, self.name, key),
            from_email="dev@omniadicta.net",
            recipient_list=["dev@omniadicta.net"],
            fail_silently=False,
        )


class User(AbstractUser):
    restaurant = models.ForeignKey(Restaurant, on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        return self.username
